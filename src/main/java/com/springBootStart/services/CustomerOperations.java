package com.springBootStart.services;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.stereotype.Service;

import com.springBootStart.model.*;
import com.springBootStart.utility.*;

@Service
public class CustomerOperations {
	

	public void addCustomer(Customer customer) {
		
		try (Session session = HibernateUtil.getSessionFactory().openSession()) {
			session.beginTransaction();
			session.save(customer);
			session.getTransaction().commit();
			
		}
		catch(Exception e) {
			System.out.println(e);
		}
	}
	
	public Customer getCustomer(int id) {
		Customer cust = new Customer();
		try (Session session = HibernateUtil.getSessionFactory().openSession()) {
		String str = "FROM Customer WHERE id = :id";
		Query query = session.createQuery(str);
		query.setParameter("id", id);
		List<Customer> result = query.list();
		cust =  result.get(0);	
	}
		catch(Exception e) {
			System.out.println(e);
		}
		return cust;
	}
	
	
	public void updateCustomerLedgerForPurchase(int id, int amount) {
		
		CustomerLedger customerLedger = new CustomerLedger();
		try (Session session = HibernateUtil.getSessionFactory().openSession()) {
			session.beginTransaction();
			Customer cust = getCustomer(id);
			customerLedger.setCustomerId(cust);
			customerLedger.setAmount(-amount);
			customerLedger.setStatus("Open");
			int balance = (-amount)+calculateBalanceForPurchase(id);
			customerLedger.setBalance(balance);
			customerLedger.setType("Purchase");
			session.save(customerLedger);
			session.getTransaction().commit();
			updatePaymentScheduleForPurchase(id, amount, customerLedger);
			
		} 
		
		catch(Exception e) {
			System.out.println(e);
		}
	}
	
	public int calculateBalanceForPurchase(int id) {
		int balance = 0;
		try (Session session = HibernateUtil.getSessionFactory().openSession()) {
			session.beginTransaction();
			String str = "select balance FROM CustomerLedger WHERE customer_id = :id order by id desc";
			Query query = session.createQuery(str);
			query.setParameter("id", id);
			List result = query.list();
			if(result.size() == 0)
			{
				return balance;
			}
			balance =  (int) result.get(0);
		}
		return balance;
	}
	
	
	public PaymentSchedules purchase(int amount) {		
		PaymentSchedules ps = new PaymentSchedules();
		ps.setStatus("Open");
		ps.setAmount(amount/4);	
		return ps;
	}
	
	
	public void updatePaymentScheduleForPurchase(int id, int amount, CustomerLedger customerLedger) {
		
		try (Session session = HibernateUtil.getSessionFactory().openSession()) {
			session.beginTransaction();
			for(int i=0;i<4;i++) {
				PaymentSchedules paymentSchedule = new PaymentSchedules();
				paymentSchedule = purchase(amount);
				int n=i+1;
				paymentSchedule.setNumber(n);
				paymentSchedule.setLedgerId(customerLedger);
				session.save(paymentSchedule);
			
			}
			session.getTransaction().commit();
		}
		catch(Exception e) {
			System.out.println(e);
		}	
	}

	public int getLedgerId(int cid, int amount) {
		int lid=0;
		amount= -amount;
		try (Session session = HibernateUtil.getSessionFactory().openSession()) {
		String str = "select id FROM CustomerLedger WHERE customer_id = :id AND type=:type AND amount=:amount";
		Query query = session.createQuery(str);
		query.setParameter("id", cid);
		query.setParameter("type", "Purchase");
		query.setParameter("amount", amount);
		List result = query.list();
		lid =  (int) result.get(0);
		}
		catch(Exception e) {
			System.out.println(e);
		}
		return lid;
	}
		
	public void updateCustomerLedgerForPayment(int id, int amount) {
		
		CustomerLedger customerLedger = new CustomerLedger();
		try (Session session = HibernateUtil.getSessionFactory().openSession()) {
			
	   		session.beginTransaction();
	   		Customer cust = getCustomer(id);
	   		int lid = getLedgerId(id, amount);
	   		int number = payment(id, lid);
			customerLedger.setCustomerId(cust);
			customerLedger.setStatus("Complete");
			customerLedger.setType("Payment");
 			customerLedger.setAmount(amount/4);
			session.save(customerLedger);
			session.getTransaction().commit();
			
			int idTemp = customerLedger.getId();
			session.beginTransaction();
			int a =customerLedger.getAmount();
			int balance = calculateBalance(id);
			balance = a + balance;
			if(number == 4) {
				customerLedger = settingPurchaseAsComplete(customerLedger, amount);
			}
			customerLedger.setBalance(balance);
			session.save(customerLedger);
			session.getTransaction().commit();
		
	}
		catch(Exception e) {
			System.out.println(e);
		}
	
		
	}
	
	
	public int payment(int customerId, int ledgerId) {
		int amount =0;
		int number =0;
		try (Session session = HibernateUtil.getSessionFactory().openSession()) {
			session.beginTransaction();
			
			Transaction transaction = null;
			String str = "FROM PaymentSchedules WHERE ledgerid = :ledgerId AND status = :status ORDER BY id ASC";
			Query query = session.createQuery(str);
			query.setParameter("ledgerId", ledgerId);
			query.setParameter("status", "Open");	
			query.setMaxResults(1);
			List result = query.list();
			PaymentSchedules ps = (PaymentSchedules)result.get(0);
			amount =ps.getAmount();
			ps.setStatus("COMPLETE");
			number = ps.getNumber();
			session.save(ps);
			session.getTransaction().commit();
			
		}
		
		catch(Exception e) {
			System.out.println(e);
		}		
		return number;
	}
	

	public CustomerLedger settingPurchaseAsComplete(CustomerLedger customerLedger, int amount) {
		
		try (Session session = HibernateUtil.getSessionFactory().openSession()) {
		
			session.beginTransaction();
			int cId = customerLedger.getCustomerId().getId();
			Transaction transaction = null;

			String str = "Select id FROM CustomerLedger WHERE customer_id = :cId AND amount=:amount ORDER BY id ASC";
			Query query = session.createQuery(str);
			query.setParameter("cId", cId);
			query.setParameter("amount", -amount);
			query.setMaxResults(1);
			List result = query.list();
			int val = (int) result.get(0);
			
			str = "Update CustomerLedger set status=:complete where id=:val";
			query = session.createQuery(str);
			query.setParameter("complete", "Complete");
			query.setParameter("val", val);
			query.executeUpdate();
		}
		
		catch(Exception e) {
			System.out.println(e);
		}
		return customerLedger;	
	}
	
	public int calculateBalance(int cid) {
		int balance=0;
		try (Session session = HibernateUtil.getSessionFactory().openSession()) {
			session.beginTransaction();
			Transaction transaction = null;
			String str = "Select balance FROM CustomerLedger WHERE customer_id = :cid order by id desc";
			Query query = session.createQuery(str);
			query.setParameter("cid", cid);
			List result = query.list();
			if(result.size()==0 || result.size()==1) {
				return balance;
			}
			balance = (int) result.get(1);
		}	
		catch(Exception e) {
			System.out.println(e);
		}
		
		return balance;
	}
	

	public boolean openTransactionCheck(int cId) {
		try (Session session = HibernateUtil.getSessionFactory().openSession()) {
			session.beginTransaction();
			Transaction transaction = null;
			String str = "FROM CustomerLedger WHERE customer_id = :cId AND status = :status AND type = :type";
			Query query = session.createQuery(str);
			query.setParameter("cId", cId);
			query.setParameter("status", "Open");
			query.setParameter("type", "purchase");
			List result = query.list();
			
			if(result.size()==0) {
				System.out.println("Customer doesn't have any open transaction");
				return false;
			}
			else {
				System.out.println("Customer has open transactions");
				return true;
				
			}
		}	
		catch(Exception e) {
			System.out.println(e);
		}
		return false;
	}
	
	public int checkBalance(int cId) {
		int balance = 0;
		try (Session session = HibernateUtil.getSessionFactory().openSession()) {
			session.beginTransaction();
			Transaction transaction = null;
			String str = "Select balance FROM CustomerLedger WHERE customer_id = :cId Order by id desc";
			Query query = session.createQuery(str);
			query.setParameter("cId", cId);
			query.setMaxResults(1);
			
			List result = query.list();
			balance = (int) result.get(0);

		}	
		catch(Exception e) {
			System.out.println(e);
		}
		System.out.println(balance);
		return balance;
	}	
	
}
