package com.springBootStart.model;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name= "CustomerLedger")
public class CustomerLedger {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	
	@Column(name="id")
	private int id;
	
	@Column(name="amount")
	private int amount;
	
	@Column(name="type")
	private String type;
	
	@ManyToOne
	@JoinColumn(name="Customer_id")
	private Customer customerId;
	
	
	@Column(name="balance")
	private int balance;
	
	@Column(name="status")
	private String status;
	
	
	@OneToMany(mappedBy="ledgerId")
	private Collection<PaymentSchedules> paymentSchedule = new ArrayList<PaymentSchedules>();
	
	public Collection<PaymentSchedules> getPaymentSchedule() {
		return paymentSchedule;
	}

	public void setPaymentSchedule(Collection<PaymentSchedules> paymentSchedule) {
		this.paymentSchedule = paymentSchedule;
	}

	public CustomerLedger() {
		
	}
	
	public void setCustomerId(Customer customerId) {
		this.customerId = customerId;
	}
	
	public Customer getCustomerId() {
		return customerId;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public int getBalance() {
		return balance;
	}

	public void setBalance(int balance) {
		this.balance = balance;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getAmount() {
		return amount;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
}
