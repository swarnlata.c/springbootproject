package com.springBootStart.model;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="Customer")
public class Customer {
	@Id 
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	
	@Column(name="Customer_id")
	private int id;
	
	@Column(name="firstName")
	private String firstName;
	
	@Column(name="lastName")
	private String lastName;
	
	@Column(name="dob")
	private String dob;
	
	@OneToMany(mappedBy="customerId")
	private Collection<CustomerLedger> customerLedger = new ArrayList<CustomerLedger>();
	
	public Collection<CustomerLedger> getCustomerLedger() {
		return customerLedger;
	}

	public void setCustomerLedger(Collection<CustomerLedger> customerLedger) {
		this.customerLedger = customerLedger;
	}

	public Customer() {
		
		
	}
	
	public Customer(String firstName, String lastName, String dob) {
		this.firstName=firstName;
		this.lastName=lastName;
		this.dob=dob;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}

}
