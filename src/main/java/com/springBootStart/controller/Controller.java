package com.springBootStart.controller;

import com.springBootStart.model.*;
import com.springBootStart.services.*;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {

	@Autowired
	private CustomerOperations customerOperation;
	
	@RequestMapping(method=RequestMethod.POST, value = "/addCustomer")
	public void addCustomer(@RequestBody Customer customer) {
		customerOperation.addCustomer(customer);
	}
	
	@RequestMapping(method=RequestMethod.POST, value = "/purchase/{id}/{amount}")
	public void updateCustomerLedgerForPurchase(@PathVariable int id, @PathVariable int amount) {
		customerOperation.updateCustomerLedgerForPurchase(id, amount); 
		
	}
	
	@RequestMapping(method=RequestMethod.POST, value ="/payment/{id}/{amount}")
	public void updateCustomerLedgerForPayment(@PathVariable int id, @PathVariable int amount) {
		customerOperation.updateCustomerLedgerForPayment(id, amount);
	}		
	
	@RequestMapping(method=RequestMethod.GET, value = "/openTransaction/{id}")	
	public boolean openTransactionCheck(@PathVariable int id) {
		return customerOperation.openTransactionCheck(id);
	}

	@RequestMapping(method=RequestMethod.GET, value = "/checkBalance/{id}")
	public int checkBalance(@PathVariable int id) {
		 return customerOperation.checkBalance(id);
	}
}

